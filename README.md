# Utilizacion de header files en C++

- `funciones.cpp`: en este archivo se escribe tanto la definicion de la función como su implementación, es decir, el código.

- `funciones.h`: en este archivo solo se escribe solo la definicion de la funcion.

- `main.cpp`: aca va el código del main.

### How to

Para compilar solo es necesario hacer:

```
# Si es en windows, la extension es exe en lugar de bin, en Linux/macOS no es necesario usar extension

$ g++ funciones.cpp main.cpp -o main.bin

# Para ejecutar

$ ./main.bin

```

### Debuggear en Visual Studio Code con multiples cpp

Al dividir el código en `main.cpp` y `funciones.cpp` se producen errores al momento de utilizar el debugger en Visual Studio Code. Esto se debe a que el debuggeo se realiza apuntando a un archivo cpp y un binario compilado a partir del source. Una forma rapida de solucionarlo es primero, desde la terminal, buildear el binario:

```
# Dentro del directorio donde está el código:
$ g++ funciones.cpp main.cpp -o main
```

Luego crear el directorio `.vscode`:

```
# Dentro del directorio donde está el código (que debe ser el mismo directorio donde estamos trabajando con Visual Studio Code)
$ mkdir .vscode
```

Luego dentro de ese directorio crear el siguiente archivo `launch.json` (testeado en MacOS Catalina):

```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Custom debug launch",
            "type": "cppdbg",
            "request": "launch",
            "program": "${fileDirname}/${fileBasenameNoExtension}",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}",
            "environment": [],
            "externalConsole": false,
            "MIMode": "lldb"
        }
    ]
}
```

Finalmente, en Visual Studio Code ir a `Run/Start Debugging` o presionar `F5`.
